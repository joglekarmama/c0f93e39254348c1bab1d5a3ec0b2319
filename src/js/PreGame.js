import Phaser from 'phaser';
import config from 'visual-config-exposer';

class PreGame extends Phaser.Scene {
  constructor() {
    super('bootGame');
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
  }

  preload() {
    console.log(config.preGame.platform);
    this.load.image('background', config.preGame.bgImg);
    this.load.image('board', config.preGame.boardImg);
    this.load.image('ground', config.preGame.ground);
    this.load.image('platform', config.preGame.platform);
    this.load.image('ball', config.preGame.ballImg);
    this.load.image('hoop', config.preGame.hoopImg);
    this.load.image('side', config.preGame.hoopEndImg);
    this.load.image('logo', config.preGame.logo);
    this.load.image('add', config.preGame.addImg);
    this.load.image('startBall', 'https://frontend.apps.playcent.com/basketball/assets/start_ball.png');
    this.load.image('energyContainer', 'https://frontend.apps.playcent.com/basketball/assets/energycontainer.png');
    this.load.image('energyBar', 'https://frontend.apps.playcent.com/basketball/assets/energybar.png');
    this.load.image('pointer', 'https://frontend.apps.playcent.com/basketball/assets/pointer.png');

    this.load.audio('score', 'https://frontend.apps.playcent.com/basketball/assets/score.wav');
    this.load.audio('backboard', 'https://frontend.apps.playcent.com/basketball/assets/backboard.wav');
    this.load.audio('whoosh', 'https://frontend.apps.playcent.com/basketball/assets/whoosh.wav');
    this.load.audio('fail', 'https://frontend.apps.playcent.com/basketball/assets/fail.wav');
    this.load.audio('spawn', 'https://frontend.apps.playcent.com/basketball/assets/spawn.wav');

    this.load.html('form', 'https://frontend.apps.playcent.com/basketball/assets/form.html');
  }

  create() {
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;

    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on('resize', this.resize, this);

    const bg = this.add.image(0, 0, 'background').setOrigin(0, 0).setScale(5);
    const logo = this.add.image(350, 300, 'logo').setScale(0.25);

    const title = this.add.text(
      this.GAME_WIDTH / 3,
      this.GAME_HEIGHT / 10,
      'BasketBall Fury',
      {
        fontFamily: 'arial',
        fontSize: '32px',
        fontStyle: 'bold',
        fill: config.game.textColor,
      }
    );

    const playBtn = this.add.text(
      this.GAME_WIDTH / 2.15,
      this.GAME_HEIGHT / 2,
      'Play',
      {
        fontSize: '32px',
        fill: config.game.textColor,
      }
    );

    const leaderBoardBtn = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 2 + 100,
      'LeaderBoards',
      {
        fontSize: '32px',
        fill: config.game.textColor,
      }
    );

    console.log(
      config.preGame.addScale,
      config.preGame.platformScale,
      config.preGame.ballScale,
      config.preGame.ballThrowScale
    );

    let hoverImage = this.add.image(100, 100, 'startBall');
    hoverImage.setVisible(false);
    hoverImage.setScale(0.05);

    playBtn.setInteractive();
    leaderBoardBtn.setInteractive();

    playBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = playBtn.x - playBtn.width;
      hoverImage.y = playBtn.y + 10;
    });
    playBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    playBtn.on('pointerdown', () => {
      // console.log('redirect to play');
      this.scene.start('instructions');
    });

    leaderBoardBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = leaderBoardBtn.x - leaderBoardBtn.width / 2.5;
      hoverImage.y = leaderBoardBtn.y + 10;
    });
    leaderBoardBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    leaderBoardBtn.on('pointerdown', () => {
      // console.log('redirect to play');
      this.scene.start('leaderBoard');
    });

    this.scale.on('resize', this.resize, this);
  }

  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;
    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;
    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default PreGame;
